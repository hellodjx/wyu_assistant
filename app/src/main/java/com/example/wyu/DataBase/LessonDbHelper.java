package com.example.wyu.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.wyu.Lesson;

public class LessonDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "Lesson.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Lesson.TABLE_NAME + " (" +
                    Lesson.COLUMN_NAME_LESSON_NAME + " TEXT," +
                    Lesson.COLUMN_NAME_TEACHER_NAME + " TEXT," +
                    Lesson.COLUMN_NAME_LOCATION + " TEXT," +
                    Lesson.COLUMN_NAME_WEEK + " TEXT," +
                    Lesson.COLUMN_NAME_DAY_OF_WEEK + " TEXT," +
                    Lesson.COLUMN_NAME_YEAR_MONTH_DAY + " TEXT," +
                    Lesson.COLUMN_NAME_INDEX_OF_LESSON_IN_THAT_DAY + " TEXT," +
                    Lesson.COLUMN_NAME_SEMESTER + " TEXT," +
                    Lesson.COLUMN_NAME_CONTENT_OF_LESSON + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Lesson.TABLE_NAME;

    public LessonDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}