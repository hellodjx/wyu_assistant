package com.example.wyu;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wyu.DataBase.LessonDbHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class MainFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private LessonAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v;
        String todayFormatDate = getTodayFormatDate();
        Log.d("today", todayFormatDate);
        Toast.makeText(getActivity(), todayFormatDate, Toast.LENGTH_SHORT).show();
        LessonDbHelper lessonDbHelper = new LessonDbHelper(getActivity());
        SQLiteDatabase db = lessonDbHelper.getWritableDatabase();
        String[] selectionArgs = {todayFormatDate};
        Cursor cur = db.rawQuery("select * from lesson where year_month_day=? order by index_of_lesson_in_that_day asc", selectionArgs);
        int todayLessonCount = 0;
        List<Lesson> lessons = new LinkedList<>();
        while(cur.moveToNext()) {
            String lesson_name = cur.getString(cur.getColumnIndexOrThrow(Lesson.COLUMN_NAME_LESSON_NAME));
            String lesson_time = cur.getString(cur.getColumnIndexOrThrow(Lesson.COLUMN_NAME_INDEX_OF_LESSON_IN_THAT_DAY));
            String lesson_location = cur.getString(cur.getColumnIndexOrThrow(Lesson.COLUMN_NAME_LOCATION));
            Lesson lesson = new Lesson();
            lesson.setmLessonName(lesson_name);
            lesson.setMlocation(lesson_location);
            lesson.setmIndexOfLessonInThatDay(lesson_time);
            lessons.add(lesson);
            todayLessonCount++;
        }

        cur.close();
        db.close();
        if (todayLessonCount <= 0){
            v = inflater.inflate(R.layout.fragment_today_no_lesson, container, false);
        }
        else{
            v = inflater.inflate(R.layout.fragment_main, container, false);
            mRecyclerView = v.findViewById(R.id.lesson_recycler_view);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            updateUI(lessons);
        }

        return v;
    }

    private void updateUI(List<Lesson> lessons) {
        mAdapter = new LessonAdapter(lessons);
        mRecyclerView.setAdapter(mAdapter);
    }

    private String getTodayFormatDate(){
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String todayFormatDate = sdf.format(today);
        return todayFormatDate;
    }

    private class LessonHolder extends RecyclerView.ViewHolder {
        private Lesson mlesson;
        private TextView mLessonName;
        private TextView mLessonTime;
        private TextView mLessonLocation;
        private ImageView mLessonBackgroundImage;

        public LessonHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_lesson, parent, false));
            mLessonName = itemView.findViewById(R.id.lesson_name);
            mLessonTime = itemView.findViewById(R.id.lesson_time);
            mLessonLocation = itemView.findViewById(R.id.lesson_location);
            mLessonBackgroundImage = itemView.findViewById(R.id.lesson_background_img);
        }

        public void bind(Lesson lesson) {
            mlesson = lesson;
            String indexOfLessonInThatDay = mlesson.getmIndexOfLessonInThatDay();
            String startOfIndexOfLessonInThatDay = indexOfLessonInThatDay.substring(0, 2);
            mLessonTime.setText(indexOfLessonInThatDay);
            mLessonName.setText(mlesson.getmLessonName());
            mLessonLocation.setText(mlesson.getMlocation());
            if (startOfIndexOfLessonInThatDay.equals("01")){
                mLessonBackgroundImage.setImageResource(R.mipmap.cloud01);
            }
            else if (startOfIndexOfLessonInThatDay.equals("03")){
                mLessonBackgroundImage.setImageResource(R.mipmap.cloud02);
            }
            else if (startOfIndexOfLessonInThatDay.equals("05")){
                mLessonBackgroundImage.setImageResource(R.mipmap.cloud03);
            }
            else if (startOfIndexOfLessonInThatDay.equals("07")){
                mLessonBackgroundImage.setImageResource(R.mipmap.cloud04);
            }
            else if (startOfIndexOfLessonInThatDay.equals("09")){
                mLessonBackgroundImage.setImageResource(R.mipmap.cloud05);
            }
            else {
                mLessonBackgroundImage.setImageResource(R.mipmap.cloud01);
            }
        }
    }

    private class LessonAdapter extends RecyclerView.Adapter<LessonHolder> {
        private List<Lesson> mLessons;

        public LessonAdapter(List<Lesson> lessons){
            mLessons = lessons;
        }

        @Override
        public LessonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            Log.d("main", "onCreateViewHolder");
            return new LessonHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(LessonHolder holder, int position) {
            Log.d("main", "onBindViewHolder");
            Lesson lesson = mLessons.get(position);
            holder.bind(lesson);
        }

        @Override
        public int getItemCount() {
            Log.d("main", mLessons.size() + "");
            return mLessons.size();
        }
    }
}
