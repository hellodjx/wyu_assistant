package com.example.wyu;

public class Score {
    private String lessonName;
    private String score;

    public Score(String lessonName, String score){
        this.lessonName = lessonName;
        this.score = score;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
