package com.example.wyu;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.wyu.DataBase.LessonDbHelper;
import com.example.wyu.ToolUtil.StudentInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.wyu.MainActivity.currentFragment;
import static com.example.wyu.ToolUtil.TimeTool.getCurrentSemester;

public class LoginFragment extends Fragment {
    private EditText mEditAccount;
    private EditText mEditPassword;
    private EditText mEditVerifyCode;
    private ImageView mVerifyCodeView;
    private Button mLoginButton;
    private String maccount;
    private String mpassword;
    private String mverify_code;
    public static final String EXTRA_WEEK = "EXTRA_WEEK";
    public static final String EXTRA_SEMESTER = "EXTRA_SEMESTER";
    Handler mainHandler, workHandler;
    HandlerThread mHandlerThread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        // 创建与主线程关联的Handler
        mainHandler = new Handler();
        mHandlerThread = new HandlerThread("handlerThread");
        mHandlerThread.start();
        workHandler = new Handler(mHandlerThread.getLooper()){
            @Override
            // 消息处理的操作
            public void handleMessage(Message msg)
            {
                Request request;
                Call call;
                OkHttpClient client = new OkHttpClient();
                String url = null;
                switch(msg.what){
                    case 1:
                        url = msg.obj.toString();
                        Response result = null;
                        Bitmap bitmap = null;
                        int retryCount = 0;
                        String sessionID = null;

                        request = new Request.Builder()
                                .url(url)
                                .addHeader("Connection","keep-alive")
                                .addHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                                .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36")
                                .addHeader("Accept-Encoding","gzip, deflate, br")
                                .addHeader("Accept-Language","zh-CN,zh;q=0.9")
                                .build();
                        Log.d("url", url);

                        while (bitmap == null && retryCount <= 3){
                            Log.d("count", retryCount + "");
                            try {
                                call = client.newCall(request);
                                result = call.execute();    //发送请求并获取响应
                                if (result != null){
                                    Headers headers = result.headers();        //得到响应头
                                    List<String> cookies = headers.values("Set-Cookie");   //得到cookies列表
                                    String session = cookies.get(0);
                                    Log.d("session", session);
                                    sessionID = session.substring(0, session.indexOf(";"));     //通过切分cookies的值得到登录所需的sessionID
                                    InputStream inputStream = result.body().byteStream();
                                    bitmap = BitmapFactory.decodeStream(inputStream);
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                            retryCount++;
                        }

                        final Bitmap finalBitmap = bitmap;
                        final String finalSessionID = sessionID;
                        // 通过主线程Handler.post方法进行在主线程的UI更新操作
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run () {
//                                    try{
//                                         bitmap = BitmapFactory.decodeStream(new BufferedInputStream(inputStream));
//                                    }
//                                    catch (Exception e){
//                                         bitmap = BitmapFactory.decodeStream(new FlushedInputStream(inputStream));
//                                    }
                                if (finalBitmap != null){
                                    mVerifyCodeView.setImageBitmap(finalBitmap);
                                }
                                else {
                                    Log.e("get verifycode img", "bitmap is null");
                                    Toast.makeText(getActivity(), R.string.failed_to_connect, Toast.LENGTH_LONG).show();
                                    mVerifyCodeView.setImageResource(R.mipmap.refresh_img);
                                }
                                StudentInfo.sessionID = finalSessionID;
                            }
                        });
                        break;

                    // 消息2
                    case 2:
                        String string_result = null;
                        if (maccount == null || mpassword == null || mverify_code == null){
                            mainHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), R.string.input_all_info, Toast.LENGTH_LONG).show();
                                }
                            });
                            break;
                        }

                        FormBody requestBody = new FormBody.Builder()
                                .add("account", maccount)
                                .add("pwd", mpassword)
                                .add("verifycode", mverify_code)
                                .build();

                        request = new Request.Builder()
                                .url("https://jxgl.wyu.edu.cn/new/login")
                                .addHeader("Cookie", StudentInfo.sessionID)
                                .post(requestBody)
                                .build();

                        call = client.newCall(request);
                        try {
                            Response response = call.execute();
                            string_result = response.body().string();
                        } catch (IOException | NullPointerException e) {
                            e.printStackTrace();
                        }
                        final String finalStringResult = string_result;

                        mainHandler.post(new Runnable() {
                            @Override
                            public void run () {
                                if (finalStringResult.contains("登录成功")){
                                    StudentInfo.is_login = true;
                                    Toast.makeText(getActivity(), R.string.login_successful, Toast.LENGTH_LONG).show();
                                    if (StudentInfo.is_update_lesson && StudentInfo.is_login){
                                        LessonDbHelper lessonDbHelper = new LessonDbHelper(getActivity());
                                        SQLiteDatabase db = lessonDbHelper.getWritableDatabase();
                                        db.execSQL("delete from lesson where semester=?", new String[]{getCurrentSemester()});
                                        for(int week = 1; week <= 20; week++){
                                            final Message get_info_msg = Message.obtain();
                                            get_info_msg.what = 3;
                                            get_info_msg.obj = "https://jxgl.wyu.edu.cn/xsgrkbcx!getDataList.action";
                                            Bundle bundle = new Bundle();
                                            bundle.putString(EXTRA_WEEK, week + "");
                                            bundle.putString(EXTRA_SEMESTER, getCurrentSemester());
                                            get_info_msg.setData(bundle);
                                            workHandler.sendMessage(get_info_msg);
                                        }
                                    }
                                    try{
                                         Fragment fragment = (Fragment) StudentInfo.lastFragmentClass.newInstance();
                                         switchFragment(fragment).commit();
                                    }
                                    catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                                else {
                                    StudentInfo.is_login = false;
                                    String errorReason = parseLoginError(finalStringResult);
                                    Toast.makeText(getActivity(), errorReason, Toast.LENGTH_LONG).show();
                                }
                                Log.d("result", finalStringResult);
                            }
                        });
                        break;
                    case 3:
                        if (!StudentInfo.is_login) {
                            Log.e("get_info", "Not Login");
                            break;
                        }
                        String lesson_info = null;
                        url = msg.obj.toString();
                        String semester = msg.getData().getString(EXTRA_SEMESTER);
                        String week = msg.getData().getString(EXTRA_WEEK);

                        FormBody formBody = new FormBody.Builder()
                                .add("xnxqdm", semester)
                                .add("sort", "kxh")
                                .add("order", "asc")
                                .add("zc", week)
                                .build();
                        request = new Request.Builder()
                                .url(url)
                                .addHeader("Cookie", StudentInfo.sessionID)
                                .post(formBody)
                                .build();

                        call = client.newCall(request);
                        try {
                            Response response = call.execute();
                            lesson_info = response.body().string();
                        }
                        catch (IOException e){
                            e.printStackTrace();
                        }
                        catch (NullPointerException e){
                            e.printStackTrace();
                        }

                        final String finalLessonInfo = lesson_info;
                        if (finalLessonInfo != null) {
                            parseLessonInfoJsonDataAndSaveInDB(finalLessonInfo, semester);
                        }
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run () {
                                if (finalLessonInfo != null) {
                                    Log.d("result", finalLessonInfo);
                                }
                                StudentInfo.is_update_lesson = false;
                            }
                        });
                        break;
                    default:
                        break;
                }
            }
        };

        if (StudentInfo.is_login){
            try{
                Fragment fragment = (Fragment) StudentInfo.lastFragmentClass.newInstance();
                switchFragment(fragment).commit();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        if (StudentInfo.is_update_lesson && StudentInfo.is_login){
            LessonDbHelper lessonDbHelper = new LessonDbHelper(getActivity());
            SQLiteDatabase db = lessonDbHelper.getWritableDatabase();
            db.execSQL("delete from lesson where semester=?", new String[]{getCurrentSemester()});
            for(int week = 1; week <= 20; week++){
                final Message get_info_msg = Message.obtain();
                get_info_msg.what = 3;
                get_info_msg.obj = "https://jxgl.wyu.edu.cn/xsgrkbcx!getDataList.action";
                Bundle bundle = new Bundle();
                bundle.putString(EXTRA_WEEK, week + "");
                bundle.putString(EXTRA_SEMESTER, getCurrentSemester());
                get_info_msg.setData(bundle);
                workHandler.sendMessage(get_info_msg);
            }
            try{
                Fragment fragment = (Fragment) StudentInfo.lastFragmentClass.newInstance();
                switchFragment(fragment).commit();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        final Message msg = Message.obtain();
        msg.what = 1;
        msg.obj = "https://jxgl.wyu.edu.cn/yzm?d=" + System.currentTimeMillis();
        workHandler.sendMessage(msg);

        mEditAccount = v.findViewById(R.id.account);
        mEditPassword = v.findViewById(R.id.password);
        mEditVerifyCode = v.findViewById(R.id.edit_verify_code);
        mVerifyCodeView = v.findViewById(R.id.verify_code);
        mLoginButton = v.findViewById(R.id.login);

        mEditAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                maccount = s.toString();
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mEditPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mpassword = s.toString();
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mEditVerifyCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mverify_code = s.toString();
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mLogin = new Login();
//                mLogin.execute("https://jxgl.wyu.edu.cn/new/login", maccount, mpassword, mverify_code, msessionID);
                final Message login_msg = Message.obtain();
                login_msg.what = 2;
                login_msg.obj = "https://jxgl.wyu.edu.cn/new/login";
                workHandler.sendMessage(login_msg);
            }
        });
        mVerifyCodeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Message msg = Message.obtain();
                msg.what = 1;
                msg.obj = "https://jxgl.wyu.edu.cn/yzm?d=" + System.currentTimeMillis();
                workHandler.sendMessage(msg);
            }
        });

        return v;
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    private String parseLoginError(String response){
        int start = response.indexOf("\"message\"") + 11;
        int end = response.indexOf("}") - 1;
        String errorReason = response.substring(start, end);
        return errorReason;
    }

    //感谢掌上邑大的代码
    private void parseLessonInfoJsonDataAndSaveInDB(String jsonData, String semester){
        LessonDbHelper lessonDbHelper = new LessonDbHelper(getActivity());
        SQLiteDatabase db = lessonDbHelper.getWritableDatabase();

        try {
            jsonData += ']';
            jsonData = '[' + jsonData;
            JSONArray jsonArray = new JSONArray(jsonData);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONArray retArray = jsonObject.getJSONArray("rows");
                for (int j = 0; j < retArray.length(); j++) {
                    JSONObject retobject = retArray.getJSONObject(j);
                    String jxcdmc = retobject.getString("jxcdmc");
                    String teaxms = retobject.getString("teaxms");
                    String xq = retobject.getString("xq");
                    String jcdm = retobject.getString("jcdm");
                    String kcmc = retobject.getString("kcmc");
                    String zc = retobject.getString("zc");
                    String jxbmc = retobject.getString("jxbmc");
                    String sknrjj = retobject.getString("sknrjj");
                    String pkrq = retobject.getString("pkrq");
                    Log.d("lesson_name", kcmc);
                    try{
                        db.execSQL("insert into lesson values(?,?,?,?,?,?,?,?,?)", new String[]
                                {kcmc, teaxms, jxcdmc, zc, xq, pkrq, jcdm, semester, sknrjj});
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        db.close();
    }

    private FragmentTransaction switchFragment(Fragment targetFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                .beginTransaction();
        if (!targetFragment.isAdded()) {
            //第一次使用switchFragment()时currentFragment为null，所以要判断一下
            if (currentFragment != null) {
                transaction.hide(currentFragment);
            }
            transaction.add(R.id.content_frame, targetFragment,targetFragment.getClass().getName());

        } else {
            transaction
                    .hide(currentFragment)
                    .show(targetFragment);
        }
        currentFragment = targetFragment;
        return transaction;
    }
}
