package com.example.wyu;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wyu.ToolUtil.StudentInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.wyu.LoginFragment.EXTRA_SEMESTER;
import static com.example.wyu.MainActivity.currentFragment;

public class ScoreFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    Handler mainHandler, workHandler;
    HandlerThread mHandlerThread;
    String murl;
    String mSemester;
    private RecyclerView mRecyclerView;
    private ArrayAdapter<String> mAdapter;
    private List<String> mAllSemesters;
    private List<Score> scores;
    private ScoreAdapter mScoreAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_score, container, false);
        murl = "https://jxgl.wyu.edu.cn/xskccjxx!getDataList.action";

        mAllSemesters = new ArrayList<String>();
        // TODO 不要硬编码学年学期
        mAllSemesters.add("202001");
        mAllSemesters.add("202002");
        mAllSemesters.add("201901");
        mAllSemesters.add("201902");
        mAllSemesters.add("201801");
        mAllSemesters.add("201802");
        Spinner spinner = (Spinner) v.findViewById(R.id.planets_spinner);

        mAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spiner_text_item, mAllSemesters);
//        mAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(mAdapter);
        spinner.setOnItemSelectedListener(this);

        Score one = new Score("test1", "60");
        Score two = new Score("test2", "100");
        scores = new LinkedList<>();
//        scores.add(one);
//        scores.add(two);
        mRecyclerView = v.findViewById(R.id.score_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        // 创建与主线程关联的Handler
        mainHandler = new Handler();
        mHandlerThread = new HandlerThread("handlerThread");
        mHandlerThread.start();
        workHandler = new Handler(mHandlerThread.getLooper()){
            @Override
            // 消息处理的操作
            public void handleMessage(Message msg)
            {
                Request request;
                Call call;
                OkHttpClient client = new OkHttpClient();
                String scoreInfo = null;
                switch(msg.what){
                    case 10:
                        Response result = null;
                        String semester = msg.getData().getString(EXTRA_SEMESTER);

                        FormBody requestBody = new FormBody.Builder()
                                .add("xnxqdm", semester)
                                .add("sort", "xnxqdm")
                                .add("order", "asc")
                                .build();

                        request = new Request.Builder()
                                .url(murl)
                                .post(requestBody)
                                .addHeader("Cookie", StudentInfo.sessionID)
                                .build();
                        call = client.newCall(request);
                        Log.d("url", murl);
                        try {
                            result = call.execute();
                        }
                        catch (IOException e){
                            e.printStackTrace();
                        }

                        try{
                            scoreInfo = result.body().string();
                            Log.d("socre", scoreInfo);
                        }
                        catch (IOException e){
                            e.printStackTrace();
                        }
                        final String finalScoreInfo = scoreInfo;

                        // 通过主线程Handler.post方法进行在主线程的UI更新操作
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                parseJSONWithJSONObject(finalScoreInfo);
                                updateUI();
                            }
                        });
                        break;
                    default:
                        break;
                    }
                }
            };

        if (StudentInfo.is_login && StudentInfo.sessionID != null){
            final Message msg = Message.obtain();
            msg.what = 10;
            Bundle bundle = new Bundle();
            mSemester = mAllSemesters.get(0);
            bundle.putString(EXTRA_SEMESTER, mSemester);
            msg.setData(bundle);
            workHandler.sendMessage(msg);
        }
        else {
            Toast.makeText(getActivity(), R.string.need_login, Toast.LENGTH_LONG).show();
            StudentInfo.lastFragmentClass = ScoreFragment.class;
            try{
                Fragment fragment = LoginFragment.class.newInstance();
                switchFragment(fragment).commit();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        return v;
    }

    private void updateUI(){
        mScoreAdapter = new ScoreAdapter(scores);
        mRecyclerView.setAdapter(mScoreAdapter);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        Log.d("spinner", pos + "");
        mSemester = mAllSemesters.get(pos);
        if (StudentInfo.is_login && StudentInfo.sessionID != null){
            final Message msg = Message.obtain();
            msg.what = 10;
            Bundle bundle = new Bundle();
            bundle.putString(EXTRA_SEMESTER, mSemester);
            msg.setData(bundle);
            workHandler.sendMessage(msg);
        }
        else {
            Toast.makeText(getActivity(), R.string.need_login, Toast.LENGTH_LONG).show();
            StudentInfo.lastFragmentClass = ScoreFragment.class;
            try{
                Fragment fragment = LoginFragment.class.newInstance();
                switchFragment(fragment).commit();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    private void parseJSONWithJSONObject(String jsonData) {
        scores.clear();
        try {
            jsonData += ']';
            jsonData = '[' + jsonData;
            JSONArray jsonArray = new JSONArray(jsonData);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONArray retArray = jsonObject.getJSONArray("rows");
                for (int j = 0; j < retArray.length(); j++) {
                    JSONObject retobject = retArray.getJSONObject(j);
                    String kcmc = retobject.getString("kcmc");//课程名称
                    String zcj = "**";
                    String cjjd = "**";
                    //如果没有进行评教这里会出问题，所以捕捉异常
                    try {
                        zcj = retobject.getString("zcj");//总成绩
                        cjjd = retobject.getString("cjjd");//绩点
                    } catch (Exception e) {
                        cjjd = "**";
                        zcj = "**";
                    }
                    String xf = retobject.getString("xf");//学分
                    String xdfsmc = retobject.getString("xdfsmc");//修读方式
                    String kcbh = retobject.getString("kcbh");//课程编号
                    String zxs = retobject.getString("zxs");//学时
                    String cjfsmc = retobject.getString("cjfsmc");//成绩方式
                    Score score = new Score(kcmc, zcj);
                    scores.add(score);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class ScoreHolder extends RecyclerView.ViewHolder {
        private TextView mLessonName;
        private TextView mScore;

        public ScoreHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_score, parent, false));
            mLessonName = itemView.findViewById(R.id.score_lesson_name);
            mScore = itemView.findViewById(R.id.score);
        }

        public void bind(Score score) {
            Log.d("score", "bind");
            mScore.setText(score.getScore());
            mLessonName.setText(score.getLessonName());
        }
    }

    private class ScoreAdapter extends RecyclerView.Adapter<ScoreHolder> {
        private List<Score> scores;

        public ScoreAdapter(List<Score> scores){
            this.scores = scores;
        }

        @Override
        public ScoreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new ScoreHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(ScoreHolder holder, int position) {
            Score score = scores.get(position);
            holder.bind(score);
        }

        @Override
        public int getItemCount() {
            Log.d("score", scores.size() + "");
            return scores.size();
        }
    }

    private FragmentTransaction switchFragment(Fragment targetFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                .beginTransaction();
        if (!targetFragment.isAdded()) {
            //第一次使用switchFragment()时currentFragment为null，所以要判断一下
            if (currentFragment != null) {
                transaction.hide(currentFragment);
            }
            transaction.add(R.id.content_frame, targetFragment,targetFragment.getClass().getName());

        } else {
            transaction
                    .hide(currentFragment)
                    .show(targetFragment);
        }
        currentFragment = targetFragment;
        return transaction;
    }
}
