package com.example.wyu;

import android.text.format.DateFormat;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Lesson {
    @SerializedName("kcmc")
    private String mLessonName;
    @SerializedName("teaxms")
    private String mTeacherName;
    @SerializedName("jxcdmc")
    private String mlocation;
    private Date mDate;
    private String mYearMonthDay;
    @SerializedName("zc")
    private String mWeek;
    private String mDayOfWeek;
    @SerializedName("jcdm")
    private String mIndexOfLessonInThatDay;
    private String mSemester;
    private String mContentOfLesson;

    public static final String TABLE_NAME = "lesson";
    public static final String COLUMN_NAME_LESSON_NAME = "lesson_name";
    public static final String COLUMN_NAME_TEACHER_NAME = "teacher_name";
    public static final String COLUMN_NAME_LOCATION = "location";
    public static final String COLUMN_NAME_WEEK = "week";
    public static final String COLUMN_NAME_DAY_OF_WEEK = "day_of_week";
    public static final String COLUMN_NAME_YEAR_MONTH_DAY = "year_month_day";
    public static final String COLUMN_NAME_INDEX_OF_LESSON_IN_THAT_DAY = "index_of_lesson_in_that_day";
    public static final String COLUMN_NAME_SEMESTER = "semester";
    public static final String COLUMN_NAME_CONTENT_OF_LESSON = "content_of_lesson";

    public String getmDayOfWeek() {
        return mDayOfWeek;
    }

    public void setmDayOfWeek(String mDayOfWeek) {
        this.mDayOfWeek = mDayOfWeek;
    }

    public String getmSemester() {
        return mSemester;
    }

    public void setmSemester(String mSemester) {
        this.mSemester = mSemester;
    }

    public String getmContentOfLesson() {
        return mContentOfLesson;
    }

    public void setmContentOfLesson(String mContentOfLesson) {
        this.mContentOfLesson = mContentOfLesson;
    }

    public String getmLessonName() {
        return mLessonName;
    }

    public void setmLessonName(String mLessonName) {
        this.mLessonName = mLessonName;
    }

    public String getmTeacherName() {
        return mTeacherName;
    }

    public void setmTeacherName(String mTeacherName) {
        this.mTeacherName = mTeacherName;
    }

    public String getMlocation() {
        return mlocation;
    }

    public void setMlocation(String mlocation) {
        this.mlocation = mlocation;
    }

    public Date getmDate() {
        return mDate;
    }

    public String getmWeek() {
        return mWeek;
    }

    public void setmWeek(String mWeek) {
        this.mWeek = mWeek;
    }

    public String getmIndexOfLessonInThatDay() {
        return mIndexOfLessonInThatDay;
    }

    public void setmIndexOfLessonInThatDay(String mIndexOfLessonInThatDay) {
        this.mIndexOfLessonInThatDay = mIndexOfLessonInThatDay;
    }

    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }

    public String getmYearMonthDay() {
        return mYearMonthDay;
    }

    public void setmYearMonthDay(String mYearMonthDay) {
        this.mYearMonthDay = mYearMonthDay;
    }

    public Lesson(){
        mDate = new Date();
        mYearMonthDay = (String) DateFormat.format("MMMM dd日, kk:mm", mDate);
    }

}
