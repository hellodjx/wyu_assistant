package com.example.wyu.ToolUtil;

import android.util.Log;

import java.util.Calendar;

public class TimeTool {
    public static String getCurrentSemester(){
        Calendar calendar = Calendar.getInstance();
        String currentSemester = null;
        int currentYear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH);
        Log.d("time", String.valueOf(currentYear) + String.valueOf(currentMonth));
        if (currentMonth == 1 || currentMonth == 2){
            currentSemester = currentYear - 1 + "01";
        }
        else if (currentMonth >= 3 && currentMonth <= 8){
            currentSemester = currentYear - 1 + "02";
        }
        else {
            currentSemester = currentYear + "01";
        }

        return currentSemester;
    }
}
